from django.urls import path

from .views import index, save_status, clear_status

urlpatterns = [
    path('', index, name='index'),
    path('save_status/', save_status, name='save_status'),
    path('clear_status/<int:item_id>/', clear_status, name='clear_status')
]