from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Status(models.Model):
    
    status_date = models.DateTimeField(editable=False)
    status_text = models.CharField(("status_text"), max_length=300)
    class Meta:
        db_table = u'status_list'
    
    def save(self, *args, **kwargs):
        self.status_date = timezone.now()
        return super(Status, self).save(*args, **kwargs)
