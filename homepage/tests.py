from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from .views import index, save_status, clear_status
from .models import Status

class HomepageFunctionalTest(StaticLiveServerTestCase):

    def setUp(self):
        options = Options()
        options.headless = True
        self.selenium = webdriver.Firefox(options=options)
        self.selenium.implicitly_wait(3)
        super(HomepageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(HomepageFunctionalTest, self).tearDown()

    def test_save_status_selenium(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        status = selenium.find_element_by_id('status_form')
        status.send_keys('Coba Coba')

        selenium.find_element_by_id('submit-button').click()
        self.assertIn('Coba Coba', self.selenium.page_source)

        selenium.get(self.live_server_url)
        self.assertIn('Coba Coba', self.selenium.page_source)
    
    def test_clear_status_selenium(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        status = selenium.find_element_by_id('status_form')
        status.send_keys('Coba Coba')

        selenium.find_element_by_id('submit-button').click()
        self.assertIn('Coba Coba', self.selenium.page_source)

        selenium.find_element_by_id('clear-button').click()
        self.assertNotIn('Coba Coba', self.selenium.page_source)

        selenium.get(self.live_server_url)
        self.assertNotIn('Coba Coba', self.selenium.page_source)

class HomepageUnitTest(TestCase):

    def test_homepage_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_save_status_redirects(self):
        response = Client().get('/save_status/')
        self.assertEqual(response.status_code,302)

    def test_clear_status_redirects(self):
        response = Client().get('/clear_status/1/')
        self.assertEqual(response.status_code, 302)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
        
    def test_using_save_status_func(self):
        found = resolve('/save_status/')
        self.assertEqual(found.func, save_status)

    def test_using_clear_status_func(self):
        found = resolve('/clear_status/1/')
        self.assertEqual(found.func, clear_status)

    def test_status_models_works(self):
        mock_status = Status()
        mock_status.status_text = "Santuy"
        mock_status.save()

        self.assertEqual(Status.objects.count(), 1)
        self.assertIsInstance(mock_status, Status)

        self.assertEqual(Status.objects.get(status_text="Santuy"), mock_status) 

        mock_status.delete()
    
    def test_index_contains_status(self):
        mock_status = Status()
        mock_status.status_text = "Santuy"
        mock_status.save()

        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertRegex(html_response, r'Santuy')

        mock_status.delete()

    def test_status_form_works(self):
        response = Client().post("/save_status/", {'status':'santuy'})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(Status.objects.count(), 1)

    def test_delete_button_works(self):
        mock_status = Status()
        mock_status.status_text = "Santuy"
        mock_status.save()

        self.assertEqual(Status.objects.count(), 1)

        response = Client().get("/clear_status/" + str(mock_status.id) + "/")

        self.assertEqual(Status.objects.count(), 0)
        self.assertEqual(response.status_code, 302)