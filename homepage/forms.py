from django import forms
from .models import Status
from django.utils import timezone
from datetime import datetime, date

class Add_Status(forms.Form):

        status_attrs = {'type':'status','class':'form-control','placeholder':'Enter Status','id':'status_form'}
        
        status = forms.CharField(label='Status :', required=True, max_length=300, widget=forms.TextInput(attrs=status_attrs))