from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .forms import Add_Status
from .models import Status

def index(request):
    response = {"status_form": Add_Status(), "status": Status.objects.order_by('status_date').reverse().values}
    return render(request, 'index.html', response)

def save_status(request):
    form = Add_Status(request.POST or None)

    if(request.method == 'POST' and form.is_valid()):
        status_text = form.cleaned_data['status']

        status = Status(status_text = status_text)
        status.save()
    
    return HttpResponseRedirect('/') 

def clear_status(request, item_id=None):
    status =  Status.objects.filter(id=item_id)
    if status != None:
        status.delete()

    return HttpResponseRedirect('/')